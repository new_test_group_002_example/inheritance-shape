/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kongphop.inheritanceshape;

/**
 *
 * @author Admin
 */
public class Rectangle extends Shape {

    private double width;
    private double height;

    public Rectangle(double width, double height) {
        super(width, height);
        this.width = width;
        this.height = height;
    }

    public double calArea() {
        return width * height;
    }

    @Override
    public void print() {
        System.out.println("Rectangle: width: " + width
                + ", height: " + height
                + ", area = " + calArea());
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kongphop.inheritanceshape;

/**
 *
 * @author Admin
 */
public class Triangle extends Shape {

    private double base;
    private double height;

    public Triangle(double base, double height) {
        super(base, height);
        this.base = base;
        this.height = height;
    }

    public double calArea() {
        return 0.5 * base * height;
    }

    @Override
    public void print() {
        System.out.println("Triangle: base: " + base
                + ", height: " + height
                + ", area = " + calArea());
    }
}

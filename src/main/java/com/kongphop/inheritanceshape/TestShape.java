/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kongphop.inheritanceshape;

/**
 *
 * @author Admin
 */
public class TestShape {
    public static void main(String[] args) {
        Circle circle1 = new Circle(3);
        circle1.print();
        
        Circle circle2 = new Circle(4);
        circle2.print();
        
        Triangle triangle = new Triangle(4, 3);
        triangle.print();
        
        Rectangle rectangle = new Rectangle(4, 3);
        rectangle.print();
        
        Square square = new Square(2);
        square.print();
        
        System.out.println("");
        
        Shape[] shapes = {circle1, circle2, triangle, rectangle, square};
        for(int i=0; i<shapes.length; i++){
            shapes[i].print();
            
        }
    }
    
    
}

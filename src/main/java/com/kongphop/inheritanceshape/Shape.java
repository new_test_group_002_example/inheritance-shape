/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kongphop.inheritanceshape;

/**
 *
 * @author Admin
 */
public class Shape {
    protected double x;
    protected double y;
    
    public Shape(double x, double y){
        System.out.println("Shape created");
        this.x = x;
        this.y = y;
    }
    
    public double calArea() {
        return x*y;
    }
    
    public void print(){
         System.out.println("name: x:"+x+", y "
                 +y+",area = "+calArea());
    }
}
